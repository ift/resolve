# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2020-2021 Max-Planck-Society
# Author: Philipp Arras

import configparser
from os.path import join
from tempfile import TemporaryDirectory

import nifty8 as ift
import nifty8.re as jft
import numpy as np
import pytest
import operator
from functools import partial, reduce

import jax
from jax import numpy as jnp
from jax import random

import resolve as rve
import resolve.re as jrve

from .common import setup_function, teardown_function

pmp = pytest.mark.parametrize
np.seterr(all="raise")

obs = rve.ms2observations(
    "/data/CYG-D-6680-64CH-10S.ms", "DATA", True, 0, polarizations="all"
)


class StokesAdder(jft.Model):
    def __init__(self, correlated_field_dict):
        self.cfs = correlated_field_dict

        super().__init__(
            init=reduce(operator.or_, [value.init for value in self.cfs.values()])
        )

    def __call__(self, x):
        def get_stokes(pre_stokes):
            pol_int = jnp.sqrt(sum(pre_stokes[i] ** 2 for i in range(1, 4)))
            return jnp.concatenate(
                [
                    jnp.exp(pre_stokes[:1]) * jnp.cosh(pol_int),
                    (jnp.exp(pre_stokes[:1]) * jnp.sinh(pol_int) / pol_int)
                    * pre_stokes[1:],
                ]
            )

        pre_stokes = jnp.stack([cf(x) for cf in self.cfs.values()])
        dims_remaining = pre_stokes.shape[1:]
        pre_stokes = pre_stokes.reshape((4, -1))

        stokes = jax.vmap(get_stokes, in_axes=1, out_axes=-1)(pre_stokes)
        return stokes.reshape((4,) + dims_remaining)


@pmp(
    "fname",
    [
        "cfg/cygnusa.cfg",
        "cfg/cygnusa_polarization.cfg",
        "cfg/mf.cfg",
        "cfg/cygnusa_mf.cfg",
        "cfg/cygnusa_mf_cfm.cfg",
    ],
)
def test_build_multi_frequency_skymodel(fname):
    tmp = TemporaryDirectory()
    direc = tmp.name
    cfg = configparser.ConfigParser()
    cfg.read(fname)
    op, _ = rve.sky_model_diffuse(cfg["sky"], obs)
    out = op(ift.from_random(op.domain))

    if not fname == "cfg/cygnusa_mf_cfm.cfg":  # FIXME: overflow in float32 conversion
        rve.ubik_tools.field2fits(out, join(direc, "tmp.fits"))

    key1 = op.domain.keys()

    op, _ = rve.sky_model_points(cfg["sky"], obs)
    if op is not None:
        out = op(ift.from_random(op.domain))
        rve.ubik_tools.field2fits(out, join(direc, "tmp1.fits"))

        key2 = op.domain.keys()
        assert len(set(key1) & set(key2)) == 0


@pytest.mark.skip(reason="pol_test.cfg not on funk")
def test_jax_skymodel_consistency():
    cfg = configparser.ConfigParser()
    cfg.read("pol_test.cfg")

    diffuse, additional = rve.sky_model_diffuse(cfg["sky"])
    pols_dict = dict()

    for pol_lbl in ("i", "q", "u", "v"):
        pols_dict |= {pol_lbl: dict()}
        for key in cfg["sky"].keys():
            if "stokes" + pol_lbl in key:
                pols_dict[pol_lbl] |= {
                    key.removeprefix("stokes" + pol_lbl + " diffuse space i0 "): cfg[
                        "sky"
                    ][key]
                }

    sky_shape = (int(cfg["sky"]["space npix x"]), int(cfg["sky"]["space npix y"]))
    distances = tuple(1 / npix for npix in sky_shape)

    correlated_field_dict = dict()

    for key, val in pols_dict.items():
        cfm = jft.CorrelatedFieldMaker("stokes" + key.upper() + " diffuse")
        cfm.set_amplitude_total_offset(
            offset_mean=float(val["zero mode offset"]),
            offset_std=(float(val["zero mode mean"]), float(val["zero mode stddev"])),
        )
        cfm.add_fluctuations(
            sky_shape,
            distances=distances,
            asperity=(float(val["asperity mean"]), float(val["asperity stddev"])),
            loglogavgslope=(
                float(val["loglogavgslope mean"]),
                float(val["loglogavgslope stddev"]),
            ),
            flexibility=(
                float(val["flexibility mean"]),
                float(val["flexibility stddev"]),
            ),
            fluctuations=(
                float(val["fluctuations mean"]),
                float(val["fluctuations stddev"]),
            ),
            prefix="space i0",
            non_parametric_kind="power",
        )
        correlated_field_dict |= {"prestokes " + key: cfm.finalize()}

    stokes_sky = StokesAdder(correlated_field_dict)

    pos_init = ift.from_random(diffuse.domain)
    re_pos_init = pos_init.val

    for key in re_pos_init.keys():
        if "spectrum" in key:
            re_pos_init[key] = re_pos_init[key].transpose()

    diffuse_field = diffuse(pos_init).val.reshape((4,) + sky_shape)
    stokes_field = stokes_sky(re_pos_init)

    np.testing.assert_allclose(diffuse_field, stokes_field)
