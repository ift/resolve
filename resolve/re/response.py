import numpy as np
from jax import numpy as jnp
import nifty8 as ift
from functools import partial

from ..util import dtype_float2complex
from jax.tree_util import Partial


def get_binbounds(coordinates):
    if len(coordinates) == 1:
        return np.array([-np.inf, np.inf])
    c = np.array(coordinates)
    bounds = np.empty(self.size + 1)
    bounds[1:-1] = c[:-1] + 0.5 * np.diff(c)
    bounds[0] = c[0] - 0.5 * (c[1] - c[0])
    bounds[-1] = c[-1] + 0.5 * (c[-1] - c[-2])
    return bounds


def convert_polarization(inp, inp_pol, out_pol):
    if inp_pol == ("I", "Q", "U", "V"):
        if out_pol == ("RR", "RL", "LR", "LL"):
            mat_stokes_to_circular = jnp.array(
                [[1, 0, 0, 1], [0, 1, 1, 0], [0, 1j, -1j, 0], [1, 0, 0, -1]]
            )
            return jnp.tensordot(mat_stokes_to_circular, inp, axes=([0], [0]))
        elif out_pol == ("XX", "XY", "YX", "YY"):
            mat_stokes_to_linear = jnp.array(
                [[1, 1, 0, 0], [1, -1, 0, 0], [0, 0, 1, 1], [0, 0, 1j, -1j]]
            )
            return jnp.tensordot(mat_stokes_to_linear, inp, axes=([0], [0]))
    elif inp_pol == ("I",):
        if out_pol == ("LL", "RR") or out_pol == ("XX", "YY"):
            new_shp = list(inp.shape)
            new_shp[0] = 2
            return jnp.broadcast_to(inp, new_shp)
        if len(out_pol) == 1 and out_pol[0] in ("I", "RR", "LL", "XX", "YY"):
            return inp
    err = f"conversion of polarization {inp_pol} to {out_pol} not implemented. Please implement!"
    raise NotImplementedError(err)


def InterferometryResponse(
    observation,
    sky_domain_dict,
    do_wgridding,
    epsilon,
    nthreads=1,
    verbosity=0,
    backend="ducc0",
):
    """Returns a function computing the radio interferometric response

    Parameters
    ----------
    observation : :class:`resolve.Observation`
        The observation for which the response should compute model visibilities
    sky_domain_dict: dict
        A dictionary providing information about the discretization of the sky.
    do_wgridding : bool
        Whether to perform wgridding.
    epsilon: float
        The numerical accuracy with which to evaluate the response.
    nthreads: int, optional
        The number of threads used to compute the response. Default: 1
    verbosity: int, optional
        If set to 1 prints information about the setup and performance of the
        response.
    backend : string
        If `ducc0` use ducc0 wgridder. If `finufft` use finufft to compute response.
    """
    if do_wgridding and backend == "finufft":
        raise RuntimeError("Cannot do wgridding with backend finufft.")

    npix_x = sky_domain_dict["npix_x"]
    npix_y = sky_domain_dict["npix_y"]
    pixsize_x = sky_domain_dict["pixsize_x"]
    pixsize_y = sky_domain_dict["pixsize_y"]

    n_pol = len(sky_domain_dict["pol_labels"])

    # compute bins for time and freq
    n_times = len(sky_domain_dict["times"])
    bb_times = get_binbounds(sky_domain_dict["times"])

    n_freqs = len(sky_domain_dict["freqs"])
    bb_freqs = get_binbounds(sky_domain_dict["freqs"])

    # build responses for: time binds, freq bins
    sr = []
    row_indices, freq_indices = [], []
    for t in range(n_times):
        sr_tmp, t_tmp, f_tmp = [], [], []
        if tuple(bb_times[t : t + 2]) == (-np.inf, np.inf):
            oo = observation
            tind = slice(None)
        else:
            oo, tind = observation.restrict_by_time(bb_times[t], bb_times[t + 1], True)
        for f in range(n_freqs):
            ooo, find = oo.restrict_by_freq(bb_freqs[f], bb_freqs[f + 1], True)
            if any(np.array(ooo.vis.shape) == 0):
                rrr = None
            else:
                if backend == "ducc0":
                    rrr = InterferometryResponseDucc(
                        ooo,
                        npix_x,
                        npix_y,
                        pixsize_x,
                        pixsize_y,
                        do_wgridding,
                        epsilon,
                        nthreads,
                        verbosity,
                    )
                elif backend == "finufft":
                    rrr = InterferometryResponseFinuFFT(
                        ooo, pixsize_x, pixsize_y, epsilon
                    )
                else:
                    err = f"backend must be `ducc0` or `finufft` not {backend}"
                    raise ValueError(err)

            sr_tmp.append(rrr)
            t_tmp.append(tind)
            f_tmp.append(find)
        sr.append(sr_tmp)
        row_indices.append(t_tmp)
        freq_indices.append(f_tmp)

    target_shape = (n_pol,) + tuple(observation.vis.shape[1:])
    foo = np.zeros(target_shape, np.int8)
    for pp in range(n_pol):
        for tt in range(n_times):
            for ff in range(n_freqs):
                foo[pp, row_indices[tt][ff], freq_indices[tt][ff]] = 1.0
    if np.any(foo == 0):
        raise RuntimeError("This should not happen. Please report.")

    inp_pol = tuple(sky_domain_dict["pol_labels"])
    out_pol = observation.vis.domain[0].labels

    def apply_R(sky):
        res = jnp.empty(target_shape, dtype_float2complex(sky.dtype))
        for pp in range(sky.shape[0]):
            for tt in range(sky.shape[1]):
                for ff in range(sky.shape[2]):
                    op = sr[tt][ff]
                    if op is None:
                        continue
                    inp = sky[pp, tt, ff]
                    r = op(inp)
                    res = res.at[pp, row_indices[tt][ff], freq_indices[tt][ff]].set(r)
        return convert_polarization(res, inp_pol, out_pol)

    return apply_R


def InterferometryResponseDucc(
    observation,
    npix_x,
    npix_y,
    pixsize_x,
    pixsize_y,
    do_wgridding,
    epsilon,
    nthreads=1,
    verbosity=0,
):
    from jaxbind.contrib import jaxducc0

    vol = pixsize_x * pixsize_y

    wg = jaxducc0.get_wgridder(
        pixsize_x=pixsize_x,
        pixsize_y=pixsize_y,
        npix_x=npix_x,
        npix_y=npix_y,
        epsilon=epsilon,
        do_wgridding=do_wgridding,
        nthreads=nthreads,
        verbosity=verbosity,
        flip_v=True,
    )
    wgridder = Partial(wg, observation.uvw, observation.freq)

    return lambda x: vol * wgridder(x)[0]


def InterferometryResponseFinuFFT(observation, pixsizex, pixsizey, epsilon):
    from jax_finufft import nufft2

    freq = observation.freq
    uvw = observation.uvw
    vol = pixsizex * pixsizey
    speedoflight = 299792458.0

    uvw = np.transpose((uvw[..., None] * freq / speedoflight), (0, 2, 1)).reshape(-1, 3)
    u, v, w = uvw.T

    u_finu = (2 * np.pi * u * pixsizex) % (2 * np.pi)
    v_finu = (-2 * np.pi * v * pixsizey) % (2 * np.pi)

    def apply_finufft(inp, u, v, eps):
        res = vol * nufft2(inp.astype(np.complex128), u, v, eps=eps)
        return res.reshape(-1, len(freq))

    R = Partial(apply_finufft, u=u_finu, v=v_finu, eps=epsilon)
    return R
